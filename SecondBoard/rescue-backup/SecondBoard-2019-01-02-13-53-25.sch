EESchema Schematic File Version 2
LIBS:4xxx
LIBS:4xxx_IEEE
LIBS:74xGxx
LIBS:74xx
LIBS:74xx_IEEE
LIBS:Amplifier_Audio
LIBS:Amplifier_Buffer
LIBS:Amplifier_Current
LIBS:Amplifier_Difference
LIBS:Amplifier_Instrumentation
LIBS:Amplifier_Operational
LIBS:Amplifier_Video
LIBS:Analog
LIBS:Analog_ADC
LIBS:Analog_DAC
LIBS:Analog_Switch
LIBS:Audio
LIBS:Battery_Management
LIBS:Comparator
LIBS:Connector
LIBS:Connector_Generic
LIBS:Connector_Generic_MountingPin
LIBS:Connector_Generic_Shielded
LIBS:Converter_ACDC
LIBS:Converter_DCDC
LIBS:CPLD_Altera
LIBS:CPLD_Xilinx
LIBS:CPU
LIBS:CPU_NXP_6800
LIBS:CPU_NXP_68000
LIBS:CPU_PowerPC
LIBS:Device
LIBS:Diode
LIBS:Diode_Bridge
LIBS:Diode_Laser
LIBS:Display_Character
LIBS:Display_Graphic
LIBS:Driver_Display
LIBS:Driver_FET
LIBS:Driver_LED
LIBS:Driver_Motor
LIBS:Driver_Relay
LIBS:DSP_AnalogDevices
LIBS:DSP_Freescale
LIBS:DSP_Microchip_DSPIC33
LIBS:DSP_Motorola
LIBS:DSP_Texas
LIBS:Fiber_Optic
LIBS:Filter
LIBS:FPGA_Lattice
LIBS:FPGA_Microsemi
LIBS:FPGA_Xilinx
LIBS:FPGA_Xilinx_Artix7
LIBS:FPGA_Xilinx_Kintex7
LIBS:FPGA_Xilinx_Spartan6
LIBS:FPGA_Xilinx_Virtex5
LIBS:FPGA_Xilinx_Virtex6
LIBS:FPGA_Xilinx_Virtex7
LIBS:GPU
LIBS:Graphic
LIBS:Interface
LIBS:Interface_CAN_LIN
LIBS:Interface_CurrentLoop
LIBS:Interface_Ethernet
LIBS:Interface_Expansion
LIBS:Interface_HID
LIBS:Interface_LineDriver
LIBS:Interface_Optical
LIBS:Interface_Telecom
LIBS:Interface_UART
LIBS:Interface_USB
LIBS:Isolator
LIBS:Isolator_Analog
LIBS:Jumper
LIBS:LED
LIBS:Logic_LevelTranslator
LIBS:Logic_Programmable
LIBS:MCU_AnalogDevices
LIBS:MCU_Cypress
LIBS:MCU_Espressif
LIBS:MCU_Intel
LIBS:MCU_Microchip_8051
LIBS:MCU_Microchip_ATmega
LIBS:MCU_Microchip_ATtiny
LIBS:MCU_Microchip_AVR
LIBS:MCU_Microchip_PIC10
LIBS:MCU_Microchip_PIC12
LIBS:MCU_Microchip_PIC16
LIBS:MCU_Microchip_PIC18
LIBS:MCU_Microchip_PIC24
LIBS:MCU_Microchip_PIC32
LIBS:MCU_Microchip_SAME
LIBS:MCU_Microchip_SAML
LIBS:MCU_Module
LIBS:MCU_Nordic
LIBS:MCU_NXP_ColdFire
LIBS:MCU_NXP_HC11
LIBS:MCU_NXP_HC12
LIBS:MCU_NXP_HCS12
LIBS:MCU_NXP_Kinetis
LIBS:MCU_NXP_LPC
LIBS:MCU_NXP_MAC7100
LIBS:MCU_NXP_MCore
LIBS:MCU_NXP_S08
LIBS:MCU_Parallax
LIBS:MCU_SiFive
LIBS:MCU_SiliconLabs
LIBS:MCU_ST_STM8
LIBS:MCU_ST_STM32F0
LIBS:MCU_ST_STM32F1
LIBS:MCU_ST_STM32F2
LIBS:MCU_ST_STM32F3
LIBS:MCU_ST_STM32F4
LIBS:MCU_ST_STM32F7
LIBS:MCU_ST_STM32H7
LIBS:MCU_ST_STM32L0
LIBS:MCU_ST_STM32L1
LIBS:MCU_ST_STM32L4
LIBS:MCU_ST_STM32L4+
LIBS:MCU_Texas
LIBS:MCU_Texas_MSP430
LIBS:Mechanical
LIBS:Memory_Controller
LIBS:Memory_EEPROM
LIBS:Memory_EPROM
LIBS:Memory_Flash
LIBS:Memory_NVRAM
LIBS:Memory_RAM
LIBS:Memory_ROM
LIBS:Memory_UniqueID
LIBS:Motor
LIBS:Oscillator
LIBS:Potentiometer_Digital
LIBS:power
LIBS:Power_Management
LIBS:Power_Protection
LIBS:Power_Supervisor
LIBS:pspice
LIBS:Reference_Current
LIBS:Reference_Voltage
LIBS:Regulator_Controller
LIBS:Regulator_Current
LIBS:Regulator_Linear
LIBS:Regulator_SwitchedCapacitor
LIBS:Regulator_Switching
LIBS:Relay
LIBS:Relay_SolidState
LIBS:RF
LIBS:RF_AM_FM
LIBS:RF_Amplifier
LIBS:RF_Bluetooth
LIBS:RF_GPS
LIBS:RF_Mixer
LIBS:RF_Module
LIBS:RF_RFID
LIBS:RF_Switch
LIBS:RF_WiFi
LIBS:RF_ZigBee
LIBS:Security
LIBS:Sensor
LIBS:Sensor_Audio
LIBS:Sensor_Current
LIBS:Sensor_Gas
LIBS:Sensor_Humidity
LIBS:Sensor_Magnetic
LIBS:Sensor_Motion
LIBS:Sensor_Optical
LIBS:Sensor_Pressure
LIBS:Sensor_Proximity
LIBS:Sensor_Temperature
LIBS:Sensor_Touch
LIBS:Sensor_Voltage
LIBS:Switch
LIBS:Timer
LIBS:Timer_PLL
LIBS:Timer_RTC
LIBS:Transformer
LIBS:Transistor_Array
LIBS:Transistor_BJT
LIBS:Transistor_FET
LIBS:Transistor_IGBT
LIBS:Triac_Thyristor
LIBS:Valve
LIBS:Video
LIBS:SecondBoard-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:GND #PWR01
U 1 1 5BF87AA4
P 1100 4700
F 0 "#PWR01" H 1100 4450 50  0001 C CNN
F 1 "GND" H 1100 4550 50  0000 C CNN
F 2 "" H 1100 4700 50  0001 C CNN
F 3 "" H 1100 4700 50  0001 C CNN
	1    1100 4700
	0    1    1    0   
$EndComp
Wire Wire Line
	1100 4700 1700 4700
$Comp
L Conn_01x07_Male J?
U 1 1 5C2CAA75
P 1050 1250
F 0 "J?" H 1050 1650 50  0000 C CNN
F 1 "Conn_01x07_Male" H 1050 850 50  0000 C CNN
F 2 "" H 1050 1250 50  0001 C CNN
F 3 "" H 1050 1250 50  0001 C CNN
	1    1050 1250
	1    0    0    -1  
$EndComp
$Comp
L R R?
U 1 1 5C2CAAE9
P 1550 950
F 0 "R?" V 1630 950 50  0000 C CNN
F 1 "100K" V 1550 950 50  0000 C CNN
F 2 "" V 1480 950 50  0001 C CNN
F 3 "" H 1550 950 50  0001 C CNN
	1    1550 950 
	0    1    1    0   
$EndComp
$Comp
L R R?
U 1 1 5C2CAB12
P 2350 950
F 0 "R?" V 2430 950 50  0000 C CNN
F 1 "10K" V 2350 950 50  0000 C CNN
F 2 "" V 2280 950 50  0001 C CNN
F 3 "" H 2350 950 50  0001 C CNN
	1    2350 950 
	0    1    1    0   
$EndComp
Wire Wire Line
	1700 950  2200 950 
Wire Wire Line
	1250 950  1400 950 
$Comp
L R R?
U 1 1 5C2CACC4
P 1550 1050
F 0 "R?" V 1630 1050 50  0000 C CNN
F 1 "100K" V 1550 1050 50  0000 C CNN
F 2 "" V 1480 1050 50  0001 C CNN
F 3 "" H 1550 1050 50  0001 C CNN
	1    1550 1050
	0    1    1    0   
$EndComp
$Comp
L R R?
U 1 1 5C2CACCA
P 2350 1050
F 0 "R?" V 2430 1050 50  0000 C CNN
F 1 "10K" V 2350 1050 50  0000 C CNN
F 2 "" V 2280 1050 50  0001 C CNN
F 3 "" H 2350 1050 50  0001 C CNN
	1    2350 1050
	0    1    1    0   
$EndComp
Wire Wire Line
	1700 1050 2200 1050
Wire Wire Line
	1250 1050 1400 1050
$Comp
L R R?
U 1 1 5C2CAD9A
P 1550 1150
F 0 "R?" V 1630 1150 50  0000 C CNN
F 1 "100K" V 1550 1150 50  0000 C CNN
F 2 "" V 1480 1150 50  0001 C CNN
F 3 "" H 1550 1150 50  0001 C CNN
	1    1550 1150
	0    1    1    0   
$EndComp
$Comp
L R R?
U 1 1 5C2CADA0
P 2350 1150
F 0 "R?" V 2430 1150 50  0000 C CNN
F 1 "10K" V 2350 1150 50  0000 C CNN
F 2 "" V 2280 1150 50  0001 C CNN
F 3 "" H 2350 1150 50  0001 C CNN
	1    2350 1150
	0    1    1    0   
$EndComp
Wire Wire Line
	1700 1150 2200 1150
Wire Wire Line
	1250 1150 1400 1150
$Comp
L R R?
U 1 1 5C2CADA8
P 1550 1250
F 0 "R?" V 1630 1250 50  0000 C CNN
F 1 "100K" V 1550 1250 50  0000 C CNN
F 2 "" V 1480 1250 50  0001 C CNN
F 3 "" H 1550 1250 50  0001 C CNN
	1    1550 1250
	0    1    1    0   
$EndComp
$Comp
L R R?
U 1 1 5C2CADAE
P 2350 1250
F 0 "R?" V 2430 1250 50  0000 C CNN
F 1 "10K" V 2350 1250 50  0000 C CNN
F 2 "" V 2280 1250 50  0001 C CNN
F 3 "" H 2350 1250 50  0001 C CNN
	1    2350 1250
	0    1    1    0   
$EndComp
Wire Wire Line
	1700 1250 2200 1250
Wire Wire Line
	1250 1250 1400 1250
$Comp
L R R?
U 1 1 5C2CAE0B
P 1550 1350
F 0 "R?" V 1630 1350 50  0000 C CNN
F 1 "100K" V 1550 1350 50  0000 C CNN
F 2 "" V 1480 1350 50  0001 C CNN
F 3 "" H 1550 1350 50  0001 C CNN
	1    1550 1350
	0    1    1    0   
$EndComp
$Comp
L R R?
U 1 1 5C2CAE11
P 2350 1350
F 0 "R?" V 2430 1350 50  0000 C CNN
F 1 "10K" V 2350 1350 50  0000 C CNN
F 2 "" V 2280 1350 50  0001 C CNN
F 3 "" H 2350 1350 50  0001 C CNN
	1    2350 1350
	0    1    1    0   
$EndComp
Wire Wire Line
	1700 1350 2200 1350
Wire Wire Line
	1250 1350 1400 1350
$Comp
L R R?
U 1 1 5C2CAE19
P 1550 1450
F 0 "R?" V 1630 1450 50  0000 C CNN
F 1 "100K" V 1550 1450 50  0000 C CNN
F 2 "" V 1480 1450 50  0001 C CNN
F 3 "" H 1550 1450 50  0001 C CNN
	1    1550 1450
	0    1    1    0   
$EndComp
$Comp
L R R?
U 1 1 5C2CAE1F
P 2350 1450
F 0 "R?" V 2430 1450 50  0000 C CNN
F 1 "10K" V 2350 1450 50  0000 C CNN
F 2 "" V 2280 1450 50  0001 C CNN
F 3 "" H 2350 1450 50  0001 C CNN
	1    2350 1450
	0    1    1    0   
$EndComp
Wire Wire Line
	1700 1450 2200 1450
Wire Wire Line
	1250 1450 1400 1450
Wire Wire Line
	1250 1550 2500 1550
Wire Wire Line
	2500 1550 2500 950 
Connection ~ 2500 1050
Connection ~ 2500 1150
Connection ~ 2500 1350
Connection ~ 2500 1250
Connection ~ 2500 1450
Connection ~ 1750 950 
Connection ~ 1800 1050
Connection ~ 1850 1150
Connection ~ 1900 1250
Connection ~ 1950 1350
Connection ~ 2000 1450
Text Notes 1200 800  0    60   ~ 0
Voltage measurement
Wire Notes Line
	650  850  2750 850 
Wire Notes Line
	2750 850  2750 1900
Wire Notes Line
	2750 1900 650  1900
Wire Notes Line
	650  1900 650  850 
$Comp
L Conn_01x02 J?
U 1 1 5C2CB6F7
P 1900 3900
F 0 "J?" H 1900 4000 50  0000 C CNN
F 1 "Lipo" H 1900 3700 50  0000 C CNN
F 2 "" H 1900 3900 50  0001 C CNN
F 3 "" H 1900 3900 50  0001 C CNN
	1    1900 3900
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x02 J?
U 1 1 5C2CB752
P 2450 3900
F 0 "J?" H 2450 4000 50  0000 C CNN
F 1 "Charger" H 2450 3700 50  0000 C CNN
F 2 "" H 2450 3900 50  0001 C CNN
F 3 "" H 2450 3900 50  0001 C CNN
	1    2450 3900
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x02 J?
U 1 1 5C2CB785
P 3050 3900
F 0 "J?" H 3050 4000 50  0000 C CNN
F 1 "Killswitch" H 3050 3700 50  0000 C CNN
F 2 "" H 3050 3900 50  0001 C CNN
F 3 "" H 3050 3900 50  0001 C CNN
	1    3050 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	1700 3900 1700 3750
Wire Wire Line
	1700 3750 2850 3750
Wire Wire Line
	2250 3750 2250 3900
Wire Wire Line
	1700 3800 1600 3800
Wire Wire Line
	1600 3800 1600 4550
Connection ~ 1700 3800
Wire Wire Line
	1700 4700 1700 4000
Wire Wire Line
	1700 4200 2250 4200
Wire Wire Line
	2250 4200 2250 4000
Connection ~ 1700 4200
Wire Wire Line
	2850 3750 2850 3900
Connection ~ 2250 3750
Wire Wire Line
	2850 4000 2850 4300
Wire Wire Line
	2850 4300 4000 4300
Wire Wire Line
	1700 4400 3700 4400
Connection ~ 1700 4400
$Comp
L +24V #PWR?
U 1 1 5C2CBAD6
P 3400 4300
F 0 "#PWR?" H 3400 4150 50  0001 C CNN
F 1 "+24V" H 3400 4440 50  0000 C CNN
F 2 "" H 3400 4300 50  0001 C CNN
F 3 "" H 3400 4300 50  0001 C CNN
	1    3400 4300
	1    0    0    -1  
$EndComp
$Comp
L +BATT #PWR?
U 1 1 5C2CBB3A
P 1350 4550
F 0 "#PWR?" H 1350 4400 50  0001 C CNN
F 1 "+BATT" H 1350 4690 50  0000 C CNN
F 2 "" H 1350 4550 50  0001 C CNN
F 3 "" H 1350 4550 50  0001 C CNN
	1    1350 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	1600 4550 1350 4550
$Comp
L CP C?
U 1 1 5C2CBE35
P 3850 4500
F 0 "C?" H 3875 4600 50  0000 L CNN
F 1 "CP" H 3875 4400 50  0000 L CNN
F 2 "" H 3888 4350 50  0001 C CNN
F 3 "" H 3850 4500 50  0001 C CNN
	1    3850 4500
	0    1    1    0   
$EndComp
$Comp
L CP C?
U 1 1 5C2CBF19
P 3850 3800
F 0 "C?" H 3875 3900 50  0000 L CNN
F 1 "CP" H 3875 3700 50  0000 L CNN
F 2 "" H 3888 3650 50  0001 C CNN
F 3 "" H 3850 3800 50  0001 C CNN
	1    3850 3800
	0    1    1    0   
$EndComp
Wire Wire Line
	4000 3800 4000 4500
Connection ~ 3400 4300
Connection ~ 4000 4300
Wire Wire Line
	3700 3800 3700 4500
Connection ~ 3700 4400
$EndSCHEMATC
