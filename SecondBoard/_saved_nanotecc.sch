EESchema Schematic File Version 4
LIBS:SecondBoard-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 10
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 2500 3200 0    50   Input ~ 0
24V
Text HLabel 2500 3450 0    50   Input ~ 0
GND
Text HLabel 2500 3700 0    50   Output ~ 0
ADC
$EndSCHEMATC
