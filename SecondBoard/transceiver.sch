EESchema Schematic File Version 2
LIBS:4xxx
LIBS:4xxx_IEEE
LIBS:74xGxx
LIBS:74xx
LIBS:74xx_IEEE
LIBS:Amplifier_Audio
LIBS:Amplifier_Buffer
LIBS:Amplifier_Current
LIBS:Amplifier_Difference
LIBS:Amplifier_Instrumentation
LIBS:Amplifier_Operational
LIBS:Amplifier_Video
LIBS:Analog
LIBS:Analog_ADC
LIBS:Analog_DAC
LIBS:Analog_Switch
LIBS:Audio
LIBS:Battery_Management
LIBS:Comparator
LIBS:Connector
LIBS:Connector_Generic
LIBS:Connector_Generic_MountingPin
LIBS:Connector_Generic_Shielded
LIBS:Converter_ACDC
LIBS:Converter_DCDC
LIBS:CPLD_Altera
LIBS:CPLD_Xilinx
LIBS:CPU
LIBS:CPU_NXP_6800
LIBS:CPU_NXP_68000
LIBS:CPU_PowerPC
LIBS:Device
LIBS:Diode
LIBS:Diode_Bridge
LIBS:Diode_Laser
LIBS:Display_Character
LIBS:Display_Graphic
LIBS:Driver_Display
LIBS:Driver_FET
LIBS:Driver_LED
LIBS:Driver_Motor
LIBS:Driver_Relay
LIBS:DSP_AnalogDevices
LIBS:DSP_Freescale
LIBS:DSP_Microchip_DSPIC33
LIBS:DSP_Motorola
LIBS:DSP_Texas
LIBS:Fiber_Optic
LIBS:Filter
LIBS:FPGA_Lattice
LIBS:FPGA_Microsemi
LIBS:FPGA_Xilinx
LIBS:FPGA_Xilinx_Artix7
LIBS:FPGA_Xilinx_Kintex7
LIBS:FPGA_Xilinx_Spartan6
LIBS:FPGA_Xilinx_Virtex5
LIBS:FPGA_Xilinx_Virtex6
LIBS:FPGA_Xilinx_Virtex7
LIBS:GPU
LIBS:Graphic
LIBS:Interface
LIBS:Interface_CAN_LIN
LIBS:Interface_CurrentLoop
LIBS:Interface_Ethernet
LIBS:Interface_Expansion
LIBS:Interface_HID
LIBS:Interface_LineDriver
LIBS:Interface_Optical
LIBS:Interface_Telecom
LIBS:Interface_UART
LIBS:Interface_USB
LIBS:Isolator
LIBS:Isolator_Analog
LIBS:Jumper
LIBS:LED
LIBS:Logic_LevelTranslator
LIBS:Logic_Programmable
LIBS:MCU_AnalogDevices
LIBS:MCU_Cypress
LIBS:MCU_Espressif
LIBS:MCU_Intel
LIBS:MCU_Microchip_8051
LIBS:MCU_Microchip_ATmega
LIBS:MCU_Microchip_ATtiny
LIBS:MCU_Microchip_AVR
LIBS:MCU_Microchip_PIC10
LIBS:MCU_Microchip_PIC12
LIBS:MCU_Microchip_PIC16
LIBS:MCU_Microchip_PIC18
LIBS:MCU_Microchip_PIC24
LIBS:MCU_Microchip_PIC32
LIBS:MCU_Microchip_SAME
LIBS:MCU_Microchip_SAML
LIBS:MCU_Module
LIBS:MCU_Nordic
LIBS:MCU_NXP_ColdFire
LIBS:MCU_NXP_HC11
LIBS:MCU_NXP_HC12
LIBS:MCU_NXP_HCS12
LIBS:MCU_NXP_Kinetis
LIBS:MCU_NXP_LPC
LIBS:MCU_NXP_MAC7100
LIBS:MCU_NXP_MCore
LIBS:MCU_NXP_S08
LIBS:MCU_Parallax
LIBS:MCU_SiFive
LIBS:MCU_SiliconLabs
LIBS:MCU_ST_STM8
LIBS:MCU_ST_STM32F0
LIBS:MCU_ST_STM32F1
LIBS:MCU_ST_STM32F2
LIBS:MCU_ST_STM32F3
LIBS:MCU_ST_STM32F4
LIBS:MCU_ST_STM32F7
LIBS:MCU_ST_STM32H7
LIBS:MCU_ST_STM32L0
LIBS:MCU_ST_STM32L1
LIBS:MCU_ST_STM32L4
LIBS:MCU_ST_STM32L4+
LIBS:MCU_Texas
LIBS:MCU_Texas_MSP430
LIBS:Mechanical
LIBS:Memory_Controller
LIBS:Memory_EEPROM
LIBS:Memory_EPROM
LIBS:Memory_Flash
LIBS:Memory_NVRAM
LIBS:Memory_RAM
LIBS:Memory_ROM
LIBS:Memory_UniqueID
LIBS:Motor
LIBS:Oscillator
LIBS:Potentiometer_Digital
LIBS:power
LIBS:Power_Management
LIBS:Power_Protection
LIBS:Power_Supervisor
LIBS:pspice
LIBS:Reference_Current
LIBS:Reference_Voltage
LIBS:Regulator_Controller
LIBS:Regulator_Current
LIBS:Regulator_Linear
LIBS:Regulator_SwitchedCapacitor
LIBS:Regulator_Switching
LIBS:Relay
LIBS:Relay_SolidState
LIBS:RF
LIBS:RF_AM_FM
LIBS:RF_Amplifier
LIBS:RF_Bluetooth
LIBS:RF_GPS
LIBS:RF_Mixer
LIBS:RF_Module
LIBS:RF_RFID
LIBS:RF_Switch
LIBS:RF_WiFi
LIBS:RF_ZigBee
LIBS:Security
LIBS:Sensor
LIBS:Sensor_Audio
LIBS:Sensor_Current
LIBS:Sensor_Gas
LIBS:Sensor_Humidity
LIBS:Sensor_Magnetic
LIBS:Sensor_Motion
LIBS:Sensor_Optical
LIBS:Sensor_Pressure
LIBS:Sensor_Proximity
LIBS:Sensor_Temperature
LIBS:Sensor_Touch
LIBS:Sensor_Voltage
LIBS:Switch
LIBS:Timer
LIBS:Timer_PLL
LIBS:Timer_RTC
LIBS:Transformer
LIBS:Transistor_Array
LIBS:Transistor_BJT
LIBS:Transistor_FET
LIBS:Transistor_IGBT
LIBS:Triac_Thyristor
LIBS:Valve
LIBS:Video
LIBS:Boards
LIBS:finder_relay
LIBS:FirstBoard
LIBS:uln2803
LIBS:SecondBoard-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 19
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Interface_CAN_LIN:SN65HVD230 U?
U 1 1 5B1A6735
P 3850 3750
AR Path="/5B1A6092/5B1A6735" Ref="U?"  Part="1" 
AR Path="/5B1A9446/5B1A6735" Ref="U?"  Part="1" 
AR Path="/5C2D44AF/5C2D78B8/5B1A6735" Ref="U?"  Part="1" 
F 0 "U?" H 3750 4150 50  0000 R CNN
F 1 "SN65HVD230" H 3750 4050 50  0000 R CNN
F 2 "lib_fp:SOIC-8_3.9x4.9mm_Pitch1.27mm" H 3850 3250 50  0001 C CNN
F 3 "" H 3750 4150 50  0001 C CNN
	1    3850 3750
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5B1A6822
P 4500 3650
AR Path="/5B1A6092/5B1A6822" Ref="R?"  Part="1" 
AR Path="/5B1A9446/5B1A6822" Ref="R?"  Part="1" 
AR Path="/5C2D44AF/5C2D78B8/5B1A6822" Ref="R?"  Part="1" 
F 0 "R?" V 4580 3650 50  0000 C CNN
F 1 "60R" V 4500 3650 50  0000 C CNN
F 2 "lib_fp:R_0805_HandSoldering" V 4430 3650 50  0001 C CNN
F 3 "" H 4500 3650 50  0000 C CNN
	1    4500 3650
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5B1A6899
P 4300 3150
AR Path="/5B1A6092/5B1A6899" Ref="C?"  Part="1" 
AR Path="/5B1A9446/5B1A6899" Ref="C?"  Part="1" 
AR Path="/5C2D44AF/5C2D78B8/5B1A6899" Ref="C?"  Part="1" 
F 0 "C?" H 4325 3250 50  0000 L CNN
F 1 "1u" H 4325 3050 50  0000 L CNN
F 2 "lib_fp:C_0805_HandSoldering" H 4338 3000 50  0001 C CNN
F 3 "" H 4300 3150 50  0000 C CNN
	1    4300 3150
	1    0    0    -1  
$EndComp
$Comp
L jetport:+3V3 #PWR?
U 1 1 5B1A696A
P 3850 3000
AR Path="/5B1A6092/5B1A696A" Ref="#PWR?"  Part="1" 
AR Path="/5B1A9446/5B1A696A" Ref="#PWR?"  Part="1" 
AR Path="/5C2D44AF/5C2D78B8/5B1A696A" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3850 2850 50  0001 C CNN
F 1 "+3V3" H 3850 3140 50  0000 C CNN
F 2 "" H 3850 3000 50  0001 C CNN
F 3 "" H 3850 3000 50  0001 C CNN
	1    3850 3000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5B1A6B33
P 3850 4150
AR Path="/5B1A6092/5B1A6B33" Ref="#PWR?"  Part="1" 
AR Path="/5B1A9446/5B1A6B33" Ref="#PWR?"  Part="1" 
AR Path="/5C2D44AF/5C2D78B8/5B1A6B33" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3850 3900 50  0001 C CNN
F 1 "GND" H 3850 4000 50  0000 C CNN
F 2 "" H 3850 4150 50  0001 C CNN
F 3 "" H 3850 4150 50  0001 C CNN
	1    3850 4150
	1    0    0    -1  
$EndComp
Text HLabel 3200 3650 0    60   Input ~ 0
CAN_TX
Text HLabel 3200 3750 0    60   Output ~ 0
CAN_RX
Text HLabel 5600 4100 2    60   BiDi ~ 0
CANL
Text HLabel 5600 3500 2    60   BiDi ~ 0
CANH
$Comp
L Device:R R?
U 1 1 5B1A6C4C
P 4500 3950
AR Path="/5B1A6092/5B1A6C4C" Ref="R?"  Part="1" 
AR Path="/5B1A9446/5B1A6C4C" Ref="R?"  Part="1" 
AR Path="/5C2D44AF/5C2D78B8/5B1A6C4C" Ref="R?"  Part="1" 
F 0 "R?" V 4580 3950 50  0000 C CNN
F 1 "60R" V 4500 3950 50  0000 C CNN
F 2 "lib_fp:R_0805_HandSoldering" V 4430 3950 50  0001 C CNN
F 3 "" H 4500 3950 50  0000 C CNN
	1    4500 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	4250 3500 4250 3750
Wire Wire Line
	4250 4100 4250 3850
Wire Wire Line
	4350 3800 4350 4400
Wire Wire Line
	4350 4400 3300 4400
Wire Wire Line
	3300 4400 3300 3850
Wire Wire Line
	3300 3850 3450 3850
Wire Wire Line
	3200 3650 3450 3650
Wire Wire Line
	3200 3750 3450 3750
$Comp
L Device:R R?
U 1 1 5B1A6E25
P 3150 4100
AR Path="/5B1A6092/5B1A6E25" Ref="R?"  Part="1" 
AR Path="/5B1A9446/5B1A6E25" Ref="R?"  Part="1" 
AR Path="/5C2D44AF/5C2D78B8/5B1A6E25" Ref="R?"  Part="1" 
F 0 "R?" V 3230 4100 50  0000 C CNN
F 1 "10K" V 3150 4100 50  0000 C CNN
F 2 "lib_fp:R_0603_HandSoldering" V 3080 4100 50  0001 C CNN
F 3 "" H 3150 4100 50  0000 C CNN
	1    3150 4100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5B1A6EC8
P 3150 4250
AR Path="/5B1A6092/5B1A6EC8" Ref="#PWR?"  Part="1" 
AR Path="/5B1A9446/5B1A6EC8" Ref="#PWR?"  Part="1" 
AR Path="/5C2D44AF/5C2D78B8/5B1A6EC8" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3150 4000 50  0001 C CNN
F 1 "GND" H 3150 4100 50  0000 C CNN
F 2 "" H 3150 4250 50  0001 C CNN
F 3 "" H 3150 4250 50  0001 C CNN
	1    3150 4250
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5B1A6EE7
P 4750 3800
AR Path="/5B1A6092/5B1A6EE7" Ref="C?"  Part="1" 
AR Path="/5B1A9446/5B1A6EE7" Ref="C?"  Part="1" 
AR Path="/5C2D44AF/5C2D78B8/5B1A6EE7" Ref="C?"  Part="1" 
F 0 "C?" H 4775 3900 50  0000 L CNN
F 1 "4n7" H 4775 3700 50  0000 L CNN
F 2 "lib_fp:C_0603_HandSoldering" H 4788 3650 50  0001 C CNN
F 3 "" H 4750 3800 50  0000 C CNN
	1    4750 3800
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5B1A7002
P 4900 3800
AR Path="/5B1A6092/5B1A7002" Ref="#PWR?"  Part="1" 
AR Path="/5B1A9446/5B1A7002" Ref="#PWR?"  Part="1" 
AR Path="/5C2D44AF/5C2D78B8/5B1A7002" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4900 3550 50  0001 C CNN
F 1 "GND" H 4900 3650 50  0000 C CNN
F 2 "" H 4900 3800 50  0001 C CNN
F 3 "" H 4900 3800 50  0001 C CNN
	1    4900 3800
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3850 3000 3850 3450
$Comp
L power:GND #PWR?
U 1 1 5B1A7154
P 4000 3300
AR Path="/5B1A6092/5B1A7154" Ref="#PWR?"  Part="1" 
AR Path="/5B1A9446/5B1A7154" Ref="#PWR?"  Part="1" 
AR Path="/5C2D44AF/5C2D78B8/5B1A7154" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4000 3050 50  0001 C CNN
F 1 "GND" H 4000 3150 50  0000 C CNN
F 2 "" H 4000 3300 50  0001 C CNN
F 3 "" H 4000 3300 50  0001 C CNN
	1    4000 3300
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5B1A7171
P 4000 3150
AR Path="/5B1A6092/5B1A7171" Ref="C?"  Part="1" 
AR Path="/5B1A9446/5B1A7171" Ref="C?"  Part="1" 
AR Path="/5C2D44AF/5C2D78B8/5B1A7171" Ref="C?"  Part="1" 
F 0 "C?" H 4025 3250 50  0000 L CNN
F 1 "100n" H 4025 3050 50  0000 L CNN
F 2 "lib_fp:C_0603_HandSoldering" H 4038 3000 50  0001 C CNN
F 3 "" H 4000 3150 50  0000 C CNN
	1    4000 3150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5B1A73AE
P 4300 3300
AR Path="/5B1A6092/5B1A73AE" Ref="#PWR?"  Part="1" 
AR Path="/5B1A9446/5B1A73AE" Ref="#PWR?"  Part="1" 
AR Path="/5C2D44AF/5C2D78B8/5B1A73AE" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4300 3050 50  0001 C CNN
F 1 "GND" H 4300 3150 50  0000 C CNN
F 2 "" H 4300 3300 50  0001 C CNN
F 3 "" H 4300 3300 50  0001 C CNN
	1    4300 3300
	1    0    0    -1  
$EndComp
$Comp
L jetport:NUP2105L D?
U 1 1 5B1A766A
P 5400 3800
AR Path="/5B1A6092/5B1A766A" Ref="D?"  Part="1" 
AR Path="/5B1A9446/5B1A766A" Ref="D?"  Part="1" 
AR Path="/5C2D44AF/5C2D78B8/5B1A766A" Ref="D?"  Part="1" 
F 0 "D?" V 5600 3800 50  0000 L CNN
F 1 "NUP2105L" V 5150 3550 50  0000 L CNN
F 2 "lib_fp:SOT-23" H 5625 3750 50  0001 L CNN
F 3 "" H 5525 3925 50  0001 C CNN
	1    5400 3800
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5B1A7763
P 5600 3800
AR Path="/5B1A6092/5B1A7763" Ref="#PWR?"  Part="1" 
AR Path="/5B1A9446/5B1A7763" Ref="#PWR?"  Part="1" 
AR Path="/5C2D44AF/5C2D78B8/5B1A7763" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5600 3550 50  0001 C CNN
F 1 "GND" H 5600 3650 50  0000 C CNN
F 2 "" H 5600 3800 50  0001 C CNN
F 3 "" H 5600 3800 50  0001 C CNN
	1    5600 3800
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5200 3500 5200 3700
Connection ~ 5200 3500
Wire Wire Line
	5200 4100 5200 3900
Connection ~ 5200 4100
$Comp
L jetport:SolderJumper_3_Open JP?
U 1 1 5B1AA9D2
P 2600 3950
AR Path="/5B1A6092/5B1AA9D2" Ref="JP?"  Part="1" 
AR Path="/5B1A9446/5B1AA9D2" Ref="JP?"  Part="1" 
AR Path="/5C2D44AF/5C2D78B8/5B1AA9D2" Ref="JP?"  Part="1" 
F 0 "JP?" H 2500 3850 50  0000 C CNN
F 1 "SolderJumper_3_Open" H 2600 4060 50  0000 C CNN
F 2 "lib_fp:SOLDER-JUMPER_2-WAY" H 2600 3950 50  0001 C CNN
F 3 "" H 2600 3950 50  0001 C CNN
	1    2600 3950
	0    -1   -1   0   
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5B1AAACA
P 2600 3750
AR Path="/5B1A6092/5B1AAACA" Ref="#PWR?"  Part="1" 
AR Path="/5B1A9446/5B1AAACA" Ref="#PWR?"  Part="1" 
AR Path="/5C2D44AF/5C2D78B8/5B1AAACA" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 2600 3600 50  0001 C CNN
F 1 "+3.3V" H 2600 3890 50  0000 C CNN
F 2 "" H 2600 3750 50  0001 C CNN
F 3 "" H 2600 3750 50  0001 C CNN
	1    2600 3750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5B1AAAF0
P 2600 4150
AR Path="/5B1A6092/5B1AAAF0" Ref="#PWR?"  Part="1" 
AR Path="/5B1A9446/5B1AAAF0" Ref="#PWR?"  Part="1" 
AR Path="/5C2D44AF/5C2D78B8/5B1AAAF0" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 2600 3900 50  0001 C CNN
F 1 "GND" H 2600 4000 50  0000 C CNN
F 2 "" H 2600 4150 50  0001 C CNN
F 3 "" H 2600 4150 50  0001 C CNN
	1    2600 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 3500 5600 3500
Wire Wire Line
	5200 4100 5600 4100
Wire Wire Line
	4250 3500 5200 3500
Wire Wire Line
	4250 4100 5200 4100
Wire Wire Line
	4350 3800 4600 3800
Wire Wire Line
	2750 3950 3450 3950
Wire Wire Line
	3850 3000 4300 3000
$EndSCHEMATC
