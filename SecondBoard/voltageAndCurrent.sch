EESchema Schematic File Version 4
LIBS:SecondBoard-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 6
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 3600 3200 2    50   Output ~ 0
GND_m
Text GLabel 3600 2950 2    50   Output ~ 0
24V-m
Text HLabel 2550 2950 0    50   Input ~ 0
24V
Text HLabel 2550 3150 0    50   Input ~ 0
GND
Text HLabel 3600 2850 2    50   Output ~ 0
24V-m
Text HLabel 3600 3100 2    50   Output ~ 0
GND-m
Wire Wire Line
	3600 2850 3600 2950
Wire Wire Line
	3600 3100 3600 3200
$EndSCHEMATC
