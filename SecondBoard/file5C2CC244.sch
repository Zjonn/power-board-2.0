EESchema Schematic File Version 2
LIBS:4xxx
LIBS:4xxx_IEEE
LIBS:74xGxx
LIBS:74xx
LIBS:74xx_IEEE
LIBS:Amplifier_Audio
LIBS:Amplifier_Buffer
LIBS:Amplifier_Current
LIBS:Amplifier_Difference
LIBS:Amplifier_Instrumentation
LIBS:Amplifier_Operational
LIBS:Amplifier_Video
LIBS:Analog
LIBS:Analog_ADC
LIBS:Analog_DAC
LIBS:Analog_Switch
LIBS:Audio
LIBS:Battery_Management
LIBS:Comparator
LIBS:Connector
LIBS:Connector_Generic
LIBS:Connector_Generic_MountingPin
LIBS:Connector_Generic_Shielded
LIBS:Converter_ACDC
LIBS:Converter_DCDC
LIBS:CPLD_Altera
LIBS:CPLD_Xilinx
LIBS:CPU
LIBS:CPU_NXP_6800
LIBS:CPU_NXP_68000
LIBS:CPU_PowerPC
LIBS:Device
LIBS:Diode
LIBS:Diode_Bridge
LIBS:Diode_Laser
LIBS:Display_Character
LIBS:Display_Graphic
LIBS:Driver_Display
LIBS:Driver_FET
LIBS:Driver_LED
LIBS:Driver_Motor
LIBS:Driver_Relay
LIBS:DSP_AnalogDevices
LIBS:DSP_Freescale
LIBS:DSP_Microchip_DSPIC33
LIBS:DSP_Motorola
LIBS:DSP_Texas
LIBS:Fiber_Optic
LIBS:Filter
LIBS:FPGA_Lattice
LIBS:FPGA_Microsemi
LIBS:FPGA_Xilinx
LIBS:FPGA_Xilinx_Artix7
LIBS:FPGA_Xilinx_Kintex7
LIBS:FPGA_Xilinx_Spartan6
LIBS:FPGA_Xilinx_Virtex5
LIBS:FPGA_Xilinx_Virtex6
LIBS:FPGA_Xilinx_Virtex7
LIBS:GPU
LIBS:Graphic
LIBS:Interface
LIBS:Interface_CAN_LIN
LIBS:Interface_CurrentLoop
LIBS:Interface_Ethernet
LIBS:Interface_Expansion
LIBS:Interface_HID
LIBS:Interface_LineDriver
LIBS:Interface_Optical
LIBS:Interface_Telecom
LIBS:Interface_UART
LIBS:Interface_USB
LIBS:Isolator
LIBS:Isolator_Analog
LIBS:Jumper
LIBS:LED
LIBS:Logic_LevelTranslator
LIBS:Logic_Programmable
LIBS:MCU_AnalogDevices
LIBS:MCU_Cypress
LIBS:MCU_Espressif
LIBS:MCU_Intel
LIBS:MCU_Microchip_8051
LIBS:MCU_Microchip_ATmega
LIBS:MCU_Microchip_ATtiny
LIBS:MCU_Microchip_AVR
LIBS:MCU_Microchip_PIC10
LIBS:MCU_Microchip_PIC12
LIBS:MCU_Microchip_PIC16
LIBS:MCU_Microchip_PIC18
LIBS:MCU_Microchip_PIC24
LIBS:MCU_Microchip_PIC32
LIBS:MCU_Microchip_SAME
LIBS:MCU_Microchip_SAML
LIBS:MCU_Module
LIBS:MCU_Nordic
LIBS:MCU_NXP_ColdFire
LIBS:MCU_NXP_HC11
LIBS:MCU_NXP_HC12
LIBS:MCU_NXP_HCS12
LIBS:MCU_NXP_Kinetis
LIBS:MCU_NXP_LPC
LIBS:MCU_NXP_MAC7100
LIBS:MCU_NXP_MCore
LIBS:MCU_NXP_S08
LIBS:MCU_Parallax
LIBS:MCU_SiFive
LIBS:MCU_SiliconLabs
LIBS:MCU_ST_STM8
LIBS:MCU_ST_STM32F0
LIBS:MCU_ST_STM32F1
LIBS:MCU_ST_STM32F2
LIBS:MCU_ST_STM32F3
LIBS:MCU_ST_STM32F4
LIBS:MCU_ST_STM32F7
LIBS:MCU_ST_STM32H7
LIBS:MCU_ST_STM32L0
LIBS:MCU_ST_STM32L1
LIBS:MCU_ST_STM32L4
LIBS:MCU_ST_STM32L4+
LIBS:MCU_Texas
LIBS:MCU_Texas_MSP430
LIBS:Mechanical
LIBS:Memory_Controller
LIBS:Memory_EEPROM
LIBS:Memory_EPROM
LIBS:Memory_Flash
LIBS:Memory_NVRAM
LIBS:Memory_RAM
LIBS:Memory_ROM
LIBS:Memory_UniqueID
LIBS:Motor
LIBS:Oscillator
LIBS:Potentiometer_Digital
LIBS:power
LIBS:Power_Management
LIBS:Power_Protection
LIBS:Power_Supervisor
LIBS:pspice
LIBS:Reference_Current
LIBS:Reference_Voltage
LIBS:Regulator_Controller
LIBS:Regulator_Current
LIBS:Regulator_Linear
LIBS:Regulator_SwitchedCapacitor
LIBS:Regulator_Switching
LIBS:Relay
LIBS:Relay_SolidState
LIBS:RF
LIBS:RF_AM_FM
LIBS:RF_Amplifier
LIBS:RF_Bluetooth
LIBS:RF_GPS
LIBS:RF_Mixer
LIBS:RF_Module
LIBS:RF_RFID
LIBS:RF_Switch
LIBS:RF_WiFi
LIBS:RF_ZigBee
LIBS:Security
LIBS:Sensor
LIBS:Sensor_Audio
LIBS:Sensor_Current
LIBS:Sensor_Gas
LIBS:Sensor_Humidity
LIBS:Sensor_Magnetic
LIBS:Sensor_Motion
LIBS:Sensor_Optical
LIBS:Sensor_Pressure
LIBS:Sensor_Proximity
LIBS:Sensor_Temperature
LIBS:Sensor_Touch
LIBS:Sensor_Voltage
LIBS:Switch
LIBS:Timer
LIBS:Timer_PLL
LIBS:Timer_RTC
LIBS:Transformer
LIBS:Transistor_Array
LIBS:Transistor_BJT
LIBS:Transistor_FET
LIBS:Transistor_IGBT
LIBS:Triac_Thyristor
LIBS:Valve
LIBS:Video
LIBS:Boards
LIBS:finder_relay
LIBS:FirstBoard
LIBS:uln2803
LIBS:SecondBoard-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 9 19
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 8900 3650 3    60   Output ~ 0
VOUT
Text HLabel 8050 3000 2    60   Output ~ 0
VIN
Text HLabel 5900 3100 0    60   Output ~ 0
GNDIN
Text HLabel 5900 2700 0    60   Output ~ 0
DISABLE
Text HLabel 10100 3300 2    60   Output ~ 0
ANALOG_I_SENSE
$Comp
L SecondBoard-rescue:finder_relay- U?
U 1 1 5C2E8FB1
P 7250 2400
AR Path="/5C2DEF7C/5C2D11EF/5C2E8FB1" Ref="U?"  Part="1" 
AR Path="/5C2DEF7C/5C2D1210/5C2E8FB1" Ref="U?"  Part="1" 
AR Path="/5C2DEF7C/5C2D1238/5C2E8FB1" Ref="U?"  Part="1" 
AR Path="/5C2D6E16/5C2D1238/5C2E8FB1" Ref="U?"  Part="1" 
AR Path="/5C2DEF7C/5C2D1259/5C2E8FB1" Ref="U?"  Part="1" 
F 0 "U?" H 7250 2050 60  0000 C CNN
F 1 "finder_relay" H 7100 1800 60  0000 C CNN
F 2 "" H 7200 2150 60  0001 C CNN
F 3 "" H 7200 2150 60  0001 C CNN
	1    7250 2400
	1    0    0    -1  
$EndComp
$Comp
L SecondBoard-rescue:CP- C?
U 1 1 5C2E905D
P 8050 3250
AR Path="/5C2DEF7C/5C2D11EF/5C2E905D" Ref="C?"  Part="1" 
AR Path="/5C2DEF7C/5C2D1210/5C2E905D" Ref="C?"  Part="1" 
AR Path="/5C2DEF7C/5C2D1238/5C2E905D" Ref="C?"  Part="1" 
AR Path="/5C2D6E16/5C2D1238/5C2E905D" Ref="C?"  Part="1" 
AR Path="/5C2DEF7C/5C2D1259/5C2E905D" Ref="C?"  Part="1" 
F 0 "C?" H 8075 3350 50  0000 L CNN
F 1 "CP" H 8075 3150 50  0000 L CNN
F 2 "" H 8088 3100 50  0001 C CNN
F 3 "" H 8050 3250 50  0001 C CNN
	1    8050 3250
	1    0    0    -1  
$EndComp
$Comp
L SecondBoard-rescue:ACS712xLCTR-20A- U?
U 1 1 5C2E90CD
P 9400 3300
AR Path="/5C2DEF7C/5C2D11EF/5C2E90CD" Ref="U?"  Part="1" 
AR Path="/5C2DEF7C/5C2D1210/5C2E90CD" Ref="U?"  Part="1" 
AR Path="/5C2DEF7C/5C2D1238/5C2E90CD" Ref="U?"  Part="1" 
AR Path="/5C2D6E16/5C2D1238/5C2E90CD" Ref="U?"  Part="1" 
AR Path="/5C2DEF7C/5C2D1259/5C2E90CD" Ref="U?"  Part="1" 
F 0 "U?" H 9500 3750 50  0000 L CNN
F 1 "ACS712xLCTR-20A" H 9500 3650 50  0000 L CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 9500 2950 50  0001 L CIN
F 3 "" H 9400 3300 50  0001 C CNN
	1    9400 3300
	1    0    0    -1  
$EndComp
$Comp
L SecondBoard-rescue:GND- #PWR?
U 1 1 5C2E91AA
P 9400 3900
AR Path="/5C2DEF7C/5C2D11EF/5C2E91AA" Ref="#PWR?"  Part="1" 
AR Path="/5C2DEF7C/5C2D1210/5C2E91AA" Ref="#PWR?"  Part="1" 
AR Path="/5C2DEF7C/5C2D1238/5C2E91AA" Ref="#PWR?"  Part="1" 
AR Path="/5C2D6E16/5C2D1238/5C2E91AA" Ref="#PWR?"  Part="1" 
AR Path="/5C2DEF7C/5C2D1259/5C2E91AA" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 9400 3650 50  0001 C CNN
F 1 "GND" H 9400 3750 50  0000 C CNN
F 2 "" H 9400 3900 50  0001 C CNN
F 3 "" H 9400 3900 50  0001 C CNN
	1    9400 3900
	1    0    0    -1  
$EndComp
$Comp
L SecondBoard-rescue:+5V- #PWR?
U 1 1 5C2E91E8
P 9400 2100
AR Path="/5C2DEF7C/5C2D11EF/5C2E91E8" Ref="#PWR?"  Part="1" 
AR Path="/5C2DEF7C/5C2D1210/5C2E91E8" Ref="#PWR?"  Part="1" 
AR Path="/5C2DEF7C/5C2D1238/5C2E91E8" Ref="#PWR?"  Part="1" 
AR Path="/5C2D6E16/5C2D1238/5C2E91E8" Ref="#PWR?"  Part="1" 
AR Path="/5C2DEF7C/5C2D1259/5C2E91E8" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 9400 1950 50  0001 C CNN
F 1 "+5V" H 9400 2240 50  0000 C CNN
F 2 "" H 9400 2100 50  0001 C CNN
F 3 "" H 9400 2100 50  0001 C CNN
	1    9400 2100
	1    0    0    -1  
$EndComp
$Comp
L SecondBoard-rescue:C- C?
U 1 1 5C2E9288
P 10000 3650
AR Path="/5C2DEF7C/5C2D11EF/5C2E9288" Ref="C?"  Part="1" 
AR Path="/5C2DEF7C/5C2D1210/5C2E9288" Ref="C?"  Part="1" 
AR Path="/5C2DEF7C/5C2D1238/5C2E9288" Ref="C?"  Part="1" 
AR Path="/5C2D6E16/5C2D1238/5C2E9288" Ref="C?"  Part="1" 
AR Path="/5C2DEF7C/5C2D1259/5C2E9288" Ref="C?"  Part="1" 
F 0 "C?" H 9900 3550 50  0000 L CNN
F 1 "1nF" H 9850 3750 50  0000 L CNN
F 2 "" H 10038 3500 50  0001 C CNN
F 3 "" H 10000 3650 50  0001 C CNN
	1    10000 3650
	-1   0    0    1   
$EndComp
$Comp
L SecondBoard-rescue:C- C?
U 1 1 5C2E92F1
P 10500 2800
AR Path="/5C2DEF7C/5C2D11EF/5C2E92F1" Ref="C?"  Part="1" 
AR Path="/5C2DEF7C/5C2D1210/5C2E92F1" Ref="C?"  Part="1" 
AR Path="/5C2DEF7C/5C2D1238/5C2E92F1" Ref="C?"  Part="1" 
AR Path="/5C2D6E16/5C2D1238/5C2E92F1" Ref="C?"  Part="1" 
AR Path="/5C2DEF7C/5C2D1259/5C2E92F1" Ref="C?"  Part="1" 
F 0 "C?" H 10525 2900 50  0000 L CNN
F 1 "0.1uF" H 10525 2700 50  0000 L CNN
F 2 "" H 10538 2650 50  0001 C CNN
F 3 "" H 10500 2800 50  0001 C CNN
	1    10500 2800
	1    0    0    -1  
$EndComp
$Comp
L SecondBoard-rescue:GND- #PWR?
U 1 1 5C2E9389
P 10500 3050
AR Path="/5C2DEF7C/5C2D11EF/5C2E9389" Ref="#PWR?"  Part="1" 
AR Path="/5C2DEF7C/5C2D1210/5C2E9389" Ref="#PWR?"  Part="1" 
AR Path="/5C2DEF7C/5C2D1238/5C2E9389" Ref="#PWR?"  Part="1" 
AR Path="/5C2D6E16/5C2D1238/5C2E9389" Ref="#PWR?"  Part="1" 
AR Path="/5C2DEF7C/5C2D1259/5C2E9389" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 10500 2800 50  0001 C CNN
F 1 "GND" H 10500 2900 50  0000 C CNN
F 2 "" H 10500 3050 50  0001 C CNN
F 3 "" H 10500 3050 50  0001 C CNN
	1    10500 3050
	1    0    0    -1  
$EndComp
$Comp
L SecondBoard-rescue:DIODE- D?
U 1 1 5C2E93A7
P 6050 2900
AR Path="/5C2DEF7C/5C2D11EF/5C2E93A7" Ref="D?"  Part="1" 
AR Path="/5C2DEF7C/5C2D1210/5C2E93A7" Ref="D?"  Part="1" 
AR Path="/5C2DEF7C/5C2D1238/5C2E93A7" Ref="D?"  Part="1" 
AR Path="/5C2D6E16/5C2D1238/5C2E93A7" Ref="D?"  Part="1" 
AR Path="/5C2DEF7C/5C2D1259/5C2E93A7" Ref="D?"  Part="1" 
F 0 "D?" H 6050 3050 50  0000 C CNN
F 1 "DIODE" H 6050 2725 50  0000 C CNN
F 2 "" H 6050 2900 50  0001 C CNN
F 3 "" H 6050 2900 50  0001 C CNN
	1    6050 2900
	0    -1   -1   0   
$EndComp
Text HLabel 8050 3450 3    60   Output ~ 0
GNDIN
Wire Wire Line
	9800 3300 10100 3300
Wire Wire Line
	9400 2100 9400 2900
Wire Wire Line
	9400 3900 9400 3700
Connection ~ 9400 3800
Wire Wire Line
	8900 3650 8900 3500
Wire Wire Line
	8900 3500 9000 3500
Wire Wire Line
	9000 3100 7800 3100
Wire Wire Line
	7800 3000 8050 3000
Connection ~ 8050 3100
Wire Wire Line
	5900 2700 6600 2700
Wire Wire Line
	6600 2700 6600 2900
Connection ~ 6050 2700
Wire Wire Line
	5900 3100 6600 3100
Connection ~ 6050 3100
Wire Wire Line
	8050 3450 8050 3400
Wire Wire Line
	9400 3800 10000 3800
Wire Wire Line
	9800 3400 10000 3400
Wire Wire Line
	10500 3050 10500 2950
Wire Wire Line
	10500 2650 9400 2650
Connection ~ 9400 2650
Wire Wire Line
	10000 3400 10000 3500
$EndSCHEMATC
